<?php
// insertamos configuracion general
include_once 'includes/config.php';

// insertamos el encabezado
include 'html/header.php';

// probando clase que hizo tayron
function getHistorialFromRepoDir($getvars,$repodir,$gitlogfull,$res)
{
    echo '<p>Historial completo de cambios del repositorio <strong>' . $getvars["repo"] . '</strong></p>';
    chdir($repodir.$getvars["repo"]);
    exec($gitlogfull,$res);
    echo '<ul>';
    foreach ($res as $commit_info) {
        echo '<li>' . $commit_info . '</li>';
    }
    echo '</ul><br><p>--------------------------<BR> <strong>Fin del historial :)</strong> </p>';
}


// empezamos el body
echo '<section><article><p>';

$resultado = array();
//$resultado = shell_exec($git_log_full);

// ejecutamos el git log full y lo guardamos como resultado

/*chdir($repo_dir);
exec($git_log_full, $resultado);*/

echo '<strong>Por favor elija que repositorio para poder visualizar el historial completo:</strong><BR>';

echo '<ul>';
foreach($repositorios as $repositorio){
    echo '<li><strong><a href="/historial.php?repo=' . $repositorio . '" title="Historial completo del repositorio">' . $repositorio . '</a></strong>';
}
echo '</ul><p>&nbsp;</p>';


//empezamos el switch
switch ($_GET["repo"]) {
    case "pm_transacciones":
        getHistorialFromRepoDir($_GET,$repo_dir,$git_log_full,$resultado);
        break;
    case "pm_clientes":
        getHistorialFromRepoDir($_GET,$repo_dir,$git_log_full,$resultado);
        break;
    case "pm_historial":
        getHistorialFromRepoDir($_GET,$repo_dir,$git_log_full,$resultado);
        break;
    case "app_adix_pagamobil":
        getHistorialFromRepoDir($_GET,$repo_dir,$git_log_full,$resultado);
        break;
    default:
        echo '<div class="alert-box error"><span>Error:</span> No se escogió ningún repositorio, por favor elija uno. :)</div>';

}
// ponemos el texto
/*echo '<strong>Historial completo de cambios de los repositorio:</strong> ' . $repositorio;
echo '<ul>';
foreach ($resultado as $commit_info) {
    echo '<li>' . $commit_info . '</li>';
}
echo '</ul><br>';
echo '<p>--------------------------<BR> <strong>Fin del historial :)</strong> </p>';*/
echo '</p></article></section>';


// insertamos el pie de pagina
include 'html/footer.php';
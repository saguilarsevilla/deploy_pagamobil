<?php
// insertamos configuraciones generales
include 'includes/config.php';

// insertamos encabezado
include 'html/header.php';

// empezamos el body
echo '<section><article><p>';

// listamos variables que vamos a usar luego
$resultado_reset = array();
$resultado_pull = array();

echo '<strong>Por favor elija que repositorio quiere actualizar:</strong><BR>';

echo '<ul>';
    foreach($repositorios as $repositorio){
        echo '<li><strong><a href="/actualizar.php?repo=' . $repositorio . '" title="Actualizar el repositorio">' . $repositorio . '</a></strong>';
    }
echo '<p>&nbsp;</p>';

//creamos funcion para actualizar los repos
function doUpdateFromRepoDir($getvars,$repo_dir,$git_pull,$resultado)
{
    echo '<p>Perfecto, vamos a proceder a actualizar el repositorio ' . $_GET["repo"] . '</p><BR>';
    $timestamp = date_create();
    echo '<p>Proceso iniciado a las <strong>' . date_format($timestamp, 'Y-m-d H:i:s') . '</strong></p>';
    chdir($repo_dir.$_GET["repo"]);
    exec($git_reset, $resultado_reset);
    echo '<p><strong>Resultado de la actualización:</strong><BR>---------------------</p>';
    chdir($repo_dir.$_GET["repo"]);
    exec($git_pull, $resultado_pull);

    foreach ($resultado_pull as $line){
        echo 'pagamobil-server&#36; ' . $line . '</BR>';
    }
    echo '</p><p><strong>Proceso finalizado</strong></p>';
}

switch ($_GET["repo"]) {
    case "pm_transacciones":
        doUpdateFromRepoDir($_GET,$repo_dir,$git_pull,$resultado);
        break;

    case "pm_historial":
        doUpdateFromRepoDir($_GET,$repo_dir,$git_pull,$resultado);
        break;

    case "pm_clientes":
        doUpdateFromRepoDir($_GET,$repo_dir,$git_pull,$resultado);
        break;

    case "app_adix_pagamobil":
        doUpdateFromRepoDir($_GET,$repo_dir,$git_pull,$resultado);
        break;

    default:
        echo "<div class='alert-box error'><span>Error:</span> No escogió ningun repo</div>";

}

// cerramos el body
echo '</p></article></section>';

// insertamos pie de pagina
include 'html/footer.php';
<?php
// Configuraciones generales


// Configuraciones de errores
error_reporting(E_ALL);

// Definimos zona horaria
date_default_timezone_set('America/Monterrey');

// Variables generales
$proveedor_git = 'bitbucket.org';
$repositorios = array("pm_clientes","pm_transacciones","pm_historial","app_adix_pagamobil");
$resultado = array();
$user_git = 'pagamobil';
$repositorio = '';
//$pass_git = 'passwd';

$repo_dir = '/webs/';
$url_git = 'https://www.' . $proveedor_git . '/' . $user_git . '/' . $repositorio . '.git';
$url_repo = 'https://www.' . $proveedor_git . '/' . $user_git . '/' . $repositorio;

// comandos
$git_pull = '/usr/bin/git pull origin master';
$git_reset = '/usr/bin/git reset --hard HEAD';
$git_fetch = '/usr/bin/git fetch';
$git_log_last = "/usr/bin/git log --pretty=format:' % h -%d % s(%cr) <%an > ' -1";
$git_log = "/usr/bin/git log --pretty=format:' % h -%d % s(%cr) <%an > ' -25";
$git_log_full = "/usr/bin/git log --pretty=format:' % h -%d % s(%cr) <%an > '";
$git_chown = 'chown www-data.www-data /webs/'. $repositorio . '/ -R';
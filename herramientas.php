<?php
// insertamos configuraciones generales
include_once 'includes/config.php';

// insertamos el encabezado
include 'html/header.php';

// empezamos el body
echo '<section><article><p>';

echo '<p><strong>URL del Repositorio en</strong> ' . $proveedor_git . ' <strong>es:</strong> <a href="' . $url_git . '" title=' . $repositorio . '>' . $url_git . '</a></p>';
echo '<p><strong>Petición de nuevas opciones o bugs:</strong> <a href="' . $url_repo . '/issues/" title="Issue tracker">' . $url_repo . '/issues/</a></p>';

//cerramos el body
echo '</p></article></section>';

// insertamos el pie de pagina
include 'html/footer.php';
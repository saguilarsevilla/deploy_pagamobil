<?php
// llamamos configuración general

include 'includes/config.php';

// insertamos el header HTML
include_once 'html/header.php';

// empezamos el body

echo '<section><article><p>';

echo 'Resumen sobre repositorios de pagamobil y las últimas versiones de cada uno:';

foreach ($repositorios as $repositorio) {
    echo '<p>El repositorio <strong><a href="http://www.' . $proveedor_git . '/' . $user_git . '/' . $repositorio . '">' . $repositorio . '</a></strong> está en la versión: <BR>--------------------------<BR>';
    chdir($repo_dir.$repositorio);
    exec($git_log_last, $resultado);
        foreach ($resultado as $line) {
            echo '<a href="/historial.php?repo=' . $repositorio . '" title="Ver historial completo del repositorio">' . $line . '</a>';
            $resultado = array();
            }
    echo '</p>';

}
echo '</article>';

//$historial_git = array();
//$output = array();
//chdir($repo_dir);
//exec($git_log, $historial_git);

//var_dump($historial_git);

//// listamos las entradas del array en formato html
//echo '<article>';
//echo '<p><strong>Ultimos 25 commits para el repositorio</strong> ' . $repositorio . ':<BR>----------------------------</p>';
//echo '<ul>';
//foreach ($historial_git as $commit_info) {
//    echo '<li>' . $commit_info . '</li>';
//}
//echo '</ul><br>';
//cerramos body

echo '</p></article></section>';

// insertamos pie de pagina
include 'html/footer.php';
?>
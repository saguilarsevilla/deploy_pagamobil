<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Consola de actualización de PagaMobil</title>
    <meta name="author" content="Salvador Aguilar"/>
    <meta name="Description"
          content="Aplicacion en PHP para actualizar PagaMobil desde los repositorios git en BitBucket">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href="css/terminal.css" rel="stylesheet"/>
    <link href="css/main.css" rel="stylesheet"/>
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<header><pre><p>
            |~~\            |\  /|   |   '|
            |__//~~|/~~|/~~|| \/ |/~\|~~\||
            |   \__|\__|\__||    |\_/|__/||
                    \__|
        </p></pre>


    <h2>Consola de Actualización de PagaMobil</h2>

    <p>&nbsp;</p>
    <?php include 'nav.php'; ?>
</header>
